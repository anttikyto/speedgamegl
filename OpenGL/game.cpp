#include "game.h"
#include "renderer.h"
#include "prefab.h"
#include "color.h"
#include "Text.h"
#include <Windows.h>
#include <iostream>

Game* Game::instance = 0;

Game* Game::getInstance()
{
	if (instance == 0)
	{
		instance = new Game();
	}
	return instance;
}

void Game::GameStart(float widthMult, float heightMult)
{
	//Random
	srand(time(0));

	//Create buttons or map
	Prefab* prefabs = new Prefab();
	Color* colors = new Color();

	//button RED
	GameObject* plane1 = new GameObject("red", prefabs->button);
	plane1->transform.GetScale() = glm::vec3(0.3f*widthMult, 0.3f*heightMult, 0.3f);
	plane1->transform.GetPos() = glm::vec3(-0.6f, 0, 0);
	plane1->ChangeColor(colors->red);

	//button Green
	GameObject* plane2 = new GameObject("green", prefabs->button);
	plane2->Transform().GetScale() = glm::vec3(0.3f*widthMult, 0.3f*heightMult, 0.3);
	plane2->transform.GetPos() = glm::vec3(-0.2f, 0, 0);
	plane2->ChangeColor(colors->green);

	//button Blue
	GameObject* plane3 = new GameObject("blue", prefabs->button);
	plane3->Transform().GetScale() = glm::vec3(0.3f*widthMult, 0.3f*heightMult, 0.3);
	plane3->Transform().GetPos() = glm::vec3(0.2, 0, 0);
	plane3->ChangeColor(colors->blue);

	//button yellow
	GameObject* plane4 = new GameObject("yellow", prefabs->button);
	plane4->Transform().GetScale() = glm::vec3(0.3f*widthMult, 0.3f*heightMult, 0.3);
	plane4->transform.GetPos() = glm::vec3(0.6f, 0, 0);
	plane4->ChangeColor(colors->yellow);

	//Text 1
	Text* text = new Text("Press Enter to Start");
	text->transform.GetScale() = glm::vec3(0.1f*widthMult, 0.1f*heightMult, 1.0f);
	text->transform.GetPos() = glm::vec3(-0.99,0.9,0);
	text->ChangeColor(colors->white);

	//Text2
	Text* text2 = new Text("(Keys are: Q W E R)");
	text2->transform.GetScale() = glm::vec3(0.05f*0.5f, 0.05f*1, 1.0f);
	text2->transform.GetPos() = glm::vec3(-0.99, 0.7, 0);
	text2->ChangeColor(colors->white);
}


void Game::GameUpdate()
{
	
	if (instance->gameRunning)
	{
		clock_t stop = clock();
		Color* colors = new Color();

		double buttonTimer = (double)(stop - instance->startTime) / CLOCKS_PER_SEC;

		//Trigger new button
		if (buttonTimer > instance->timeToNextButton)
		{
			//reset timer
			instance->startTime = clock();

			//Get random Button and change color
			int randButton = rand() % 4 + 0;
			instance->correctButtonIndex = randButton;
			std::cout << randButton<<std::endl;
			GameObject* pButton = Renderer::getInstance()->gameObjects[randButton];
			switch (randButton)
			{
			case 0:
				pButton->ChangeColor(colors->redL);
				break;
			case 1:
				pButton->ChangeColor(colors->greenL);
				break;
			case 2:
				pButton->ChangeColor(colors->blueL);
				break;
			case 3:
				pButton->ChangeColor(colors->yellowL);
				break;
			default:
				break;
			}
		}

		//Q Key  RED
		if (GetAsyncKeyState(0x51) & 0x8000)
		{
			if (instance->correctButtonIndex == 0)
				Renderer::getInstance()->gameObjects[0]->ChangeColor(colors->red);
		}

		//W Key  GREEN
		if (GetAsyncKeyState(0x57) & 0x8000)
		{
			if (instance->correctButtonIndex == 1)
				Renderer::getInstance()->gameObjects[1]->ChangeColor(colors->green);
		}

		//E Key   BLUE
		if (GetAsyncKeyState(0x45) & 0x8000)
		{
			if (instance->correctButtonIndex == 2)
				Renderer::getInstance()->gameObjects[2]->ChangeColor(colors->blue);
		}

		//R Key   YELLOW
		if (GetAsyncKeyState(0x52) & 0x8000)
		{
			if (instance->correctButtonIndex == 3)
				Renderer::getInstance()->gameObjects[3]->ChangeColor(colors->yellow);
		}


	}
	else
	{
		//ENTER KEY  Start Game
		if (GetAsyncKeyState(0x0D) & 0x8000)
		{
			//Remove all text and create score text And start Game
			Renderer::getInstance()->texts.clear();
			Text* score = new Text("Score: ");
			score->transform.GetScale() = glm::vec3(0.05f, 0.1f, 1);
			score->transform.GetPos() = glm::vec3(-0.99f, 0.9f, 1);
			instance->startTime = clock();
			instance->gameRunning = true;
		}
	}
}

Game::~Game()
{

}
