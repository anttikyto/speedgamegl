#include <time.h>

#pragma once
class Game
{
public:
	static Game* getInstance();
	void GameStart(float widthMult, float heightMult);
	void GameUpdate();
	~Game();
private:
	static Game* instance;
	bool gameRunning;

	int score;
	clock_t startTime;
	bool buttonPressed;
	float timeToNextButton = 1;
	int correctButtonIndex;
};