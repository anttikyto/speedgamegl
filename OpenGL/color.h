#pragma once
#include "renderer.h"


class Color
{
public:

	//Disabled colors
	GLfloat white[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat red[4] = { 0.7f, 0.0f, 0.0f, 1.0f };
	GLfloat redL[4] = { 1.7f, 0.0f, 0.0f, 1.0f };
	GLfloat green[4] = { 0.0f, 0.7f, 0.0f, 1.0f };
	GLfloat greenL[4] = { 0.0f, 1.7f, 0.0f, 1.0f };
	GLfloat blue[4] = { 0.0f, 0.0f, 0.7f, 1.0f };
	GLfloat blueL[4] = { 0.0f, 0.0f, 1.7f, 1.0f };
	GLfloat yellow[4] = { 0.7f, 0.7f, 0.0f, 1.0f };
	GLfloat yellowL[4] = { 1.7f, 1.7f, 0.0f, 1.0f };
};