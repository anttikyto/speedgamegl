#include "gameobject.h"
#include "transform.h"
#include "texture.h"
#include "shader.h"
#include "prefab.h"
#include "renderer.h"

//Pit�� antaa parametri, jotta tiedet��n mik� Prefab on kyseess�/ Esim Button
GameObject::GameObject(std::string name, std::string finByName)
{
	Prefab* prefab = new Prefab();

	//Plane Mesh
	Vertex verticles[] =
	{
		Vertex(glm::vec3(-1, -1, 0), glm::vec2(0.0,1.0)),
		Vertex(glm::vec3(1, -1, 0), glm::vec2(1.0,1.0)),
		Vertex(glm::vec3(-1, 1, 0), glm::vec2(0.0,0.0)),

		Vertex(glm::vec3(1 , 1, 0), glm::vec2(1.0,0.0)),
		Vertex(glm::vec3(-1 , 1 , 0), glm::vec2(0.0,0.0)),
		Vertex(glm::vec3(1 , -1, 0), glm::vec2(1.0,1.0)),
	};

	//T�h�n Etsit��n Jsonista oikeat meshit ja textuurit meid�n gameObjektille / Button
	GameObject::name = name;
	GameObject::mesh = new Mesh(verticles, sizeof(verticles) / sizeof(verticles[0]));
	GameObject::shader = new Shader(".//res//basicShader");
	GameObject::texture = new Texture(prefab->GetTexture(finByName));
	Renderer::getInstance()->gameObjects.push_back(this);
}

//Change GameObject color
void GameObject::ChangeColor(GLfloat color[4])
{
	for (int i = 0; i < 4; i++)
		col[i] = color[i];
}

GameObject::~GameObject()
{
	delete(mesh);
	delete(shader);
	delete(texture);
}
