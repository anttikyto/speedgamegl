#include "Text.h"
#include "renderer.h"
#include <fstream>
#include <streambuf>

struct CHARDATA
{
	float size;
	float x;
	float y;
	float width;
	float height;
	float xOffset;
	float yOffset;
	float xAdvance;
};

std::string GetFontAtlas();
CHARDATA GetCharData(std::string atlas, unsigned char chr);

Text::Text(std::string text)
{
	std::string atlas = GetFontAtlas();

	//Help with move
	float move = 0;

	for (int i =0; i<text.length(); i++)
	{
		CHARDATA cData = GetCharData(atlas, text[i]);
		move += cData.xOffset+cData.xAdvance;
		Vertex verticles[] =
		{
			//One triangle
			Vertex(glm::vec3(-cData.width/cData.size+move, -cData.height/cData.size - cData.yOffset, 0), glm::vec2(cData.x,cData.y+cData.height)),
			Vertex(glm::vec3(cData.width/cData.size+move, -cData.height/cData.size - cData.yOffset, 0), glm::vec2(cData.x+cData.width,cData.y + cData.height)),
			Vertex(glm::vec3(-cData.width/cData.size+move, cData.height/cData.size - cData.yOffset, 0), glm::vec2(cData.x,cData.y)),

			//Second triangle
			Vertex(glm::vec3(cData.width/cData.size+move, cData.height/cData.size - cData.yOffset, 0), glm::vec2(cData.x + cData.width,cData.y)),
			Vertex(glm::vec3(-cData.width/cData.size+move,cData.height/cData.size - cData.yOffset, 0), glm::vec2(cData.x,cData.y)),
			Vertex(glm::vec3(cData.width/cData.size+move, -cData.height/cData.size - cData.yOffset, 0), glm::vec2(cData.x + cData.width,cData.y + cData.height)),
		};
		//Move and Render
		move += cData.xAdvance;
		Text::mesh.push_back(new Mesh(verticles, sizeof(verticles) / sizeof(verticles[0])));
		Text::shader = new Shader(".//res//basicShader");
		Text::texture = new Texture(".//res//sans.png"); //Replace this and get it from .fnt file
		glTexCoord2f(0.0f, 0.0f);
		transform.GetPos() = glm::vec3(0,0,0);
		Renderer::getInstance()->texts.push_back(this);
	}
}

//Change GameObject color
void Text::ChangeColor(GLfloat color[4])
{
	for (int i = 0; i < 4; i++)
		col[i] = color[i];
}

std::string GetFontAtlas()
{
	//Load atlas Data and make it to string file
	std::ifstream ifs(".//res//sans.fnt");
	std::string content((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));

	return content;
}

//Parse data from fnt file
CHARDATA GetCharData(std::string atlas, unsigned char chr)
{
	CHARDATA charData;
	int c = chr;
	std::size_t start = atlas.find("id="+std::to_string(c));
	std::string str3 = atlas.substr(start);     //Helper
	std::size_t end = str3.find("chnl=0");

	std::string final = str3.substr(0, end+6);

	//Atoi concert string to int
	int scaleW = std::atoi(atlas.substr(atlas.find("scaleW=") + 7, 3).c_str());
	int scaleH = std::atoi(atlas.substr(atlas.find("scaleH=") + 7, 3).c_str());
	int size = std::atoi(atlas.substr(atlas.find("size=") + 5, 3).c_str());

	//Stof convert string to float
	charData.size = (float)size/(float)scaleW;
	charData.x = std::stof(final.substr(final.find("x=")+2,3).c_str())/scaleW;
	charData.y = std::stof(final.substr(final.find("y=")+2, 3).c_str())/scaleH;
	charData.width = std::stof(final.substr(final.find("width=") + 6, 3).c_str())/scaleW;
	charData.height = std::stof(final.substr(final.find("height=") + 7, 3).c_str())/scaleH;
	charData.xOffset = std::stof(final.substr(final.find("xoffset=") + 8, 3).c_str()) / size;
	charData.yOffset = std::stof(final.substr(final.find("yoffset=") + 8, 3).c_str()) / size;
	charData.xAdvance = std::stof(final.substr(final.find("xadvance=") + 9, 3).c_str()) / size;

	return charData;
}

Text::~Text()
{
}
