#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <GL\glew.h>
#include "transform.h"

class Shader
{

public:
	Shader(const std::string& fileName);

	void Bind();
	void Update(const Transform& transform, GLfloat col[]);
	void SetColor(GLfloat col[]);

	virtual ~Shader();
protected:
private:
	static const unsigned int NUM_SHADER = 2;
	Shader(const Shader& other) {}
	void operator=(const Shader& other){}

	enum
	{
		TRANSFORM_U,
		COLOR_U,
		NUM_UNIFORMS
	};

	GLuint m_program;
	GLuint m_shaders[NUM_SHADER];
	GLuint m_uniforms[NUM_UNIFORMS];
};

#endif // SHADER_H