#include "renderer.h"
#include "prefab.h"
#include "game.h"
#include <iostream>
#include <ft2build.h>
#include FT_FREETYPE_H  


Renderer* Renderer::instance = 0;

Renderer* Renderer::getInstance()
{
	if (instance == 0)
	{
		instance = new Renderer();
	}
	return instance;
}

int Renderer::Render(int width, int height)
{
	float widthMult = (float)height / std::fmax((float)width, (float)height);
	float heightMult = (float)width / std::fmax((float)width, (float)height);
	
	//Init Game--Game Start
	Game::getInstance()->GameStart(widthMult, heightMult);

	//While window is open
	while (!instance->display->IsClosed())
	{
		Game::getInstance()->GameUpdate();
		//Background Color
		instance->display->Clear(0.9f, 0.3f, 0.5f, 0);
		//Foreach GameObject
		for each (GameObject* gameObject in instance->gameObjects)
		{
			//Bind Shader
			gameObject->shader->Bind();
			//Bind Texture
			gameObject->texture->Bind(0);
			//Update position and color
			gameObject->shader->Update(gameObject->transform, gameObject->col);
			//Draw GameObject
			gameObject->mesh->Draw();
		}

		for each (Text* gameObject in instance->texts)
		{
			//Bind Shader
			gameObject->shader->Bind();
			//Bind Texture
			gameObject->texture->Bind(0);
			//Update position and color
			gameObject->shader->Update(gameObject->transform, gameObject->col);
			//Draw GameObject
			for each (Mesh* mesh in gameObject->mesh)
			{
				mesh->Draw();
			}
		}
		instance->display->Update();
	}

	return 0;
}

Renderer::~Renderer()
{
}
