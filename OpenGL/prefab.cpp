#include "prefab.h"
#include "gameobject.h"
#include <sstream>
#include <fstream>
#include <iostream>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/writer.h>
#include <cstdio>

Prefab::Prefab()
{
	
}

//GET CORRECT NAME
std::string Prefab::GetName(std::string nameId)
{
	std::ifstream ifs(".//res//prefabs//prefabs.json");
	rapidjson::IStreamWrapper isw(ifs);
	rapidjson::Document d;
	d.ParseStream(isw);

	assert(d.IsArray());
	for (rapidjson::SizeType i = 0; i <d.Size(); i++) // Uses SizeType instead of size_t
	{
		if (d[i].HasMember("name"))
		{
			assert(d[i].IsObject());
			if (d[i]["name"].GetString() == nameId)
			{
				return d[i]["name"].GetString();
			}
		}
	}
}


//GET CORRECT TEXTURE
std::string Prefab::GetTexture(std::string nameId)
{
	std::ifstream ifs(".//res//prefabs//prefabs.json");
	rapidjson::IStreamWrapper isw(ifs);
	rapidjson::Document d;
	d.ParseStream(isw);

	assert(d.IsArray());
	for (rapidjson::SizeType i = 0; i <d.Size(); i++) // Uses SizeType instead of size_t
	{
		if (d[i].HasMember("texture"))
		{
			assert(d[i].IsObject());
			if (d[i]["name"].GetString() == nameId)
			{
				return d[i]["texture"].GetString();
			}
		}
	}
}

Prefab::~Prefab()
{

}
