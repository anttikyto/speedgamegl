#include "mesh.h"
#include "shader.h"
#include "texture.h"
#include <vector>
#include <string>

#pragma once
class Text
{
public:
	Text(std::string text);
	void ChangeColor(GLfloat color[4]);
	std::vector<Mesh*> mesh;
	Shader* shader;
	Texture* texture;
	GLfloat col[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	Transform transform;
	~Text();
};

