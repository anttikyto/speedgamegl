#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "mesh.h"
#include "transform.h"
#include "texture.h"
#include "shader.h"
#include <string>

class GameObject
{

public:
	GameObject(std::string name, std::string finByName);
	void ChangeColor(GLfloat color[4]);
	std::string name;
	Mesh* mesh;
	Shader* shader;
	Texture* texture;
	GLfloat col[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	Transform transform;
	inline Transform& Transform() { return transform; }

	~GameObject();
private:
};

#endif // GAMEOBJECT

