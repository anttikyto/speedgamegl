#ifndef MESH_H
#define MESH_H

#include <glm\glm.hpp>
#include <GL\glew.h>

class Vertex
{
public:
	Vertex(const glm::vec3& pos, const glm::vec2& texCoord)
	{
		this->pos = pos;
		this -> texCoord = texCoord;
	}

	inline glm::vec3* GetPos() { return &pos; }
	inline glm::vec2* GetTexCoord() { return &texCoord; }

protected:
private:
	glm::vec3 pos;
	glm::vec2 texCoord;
};

class Mesh {
private:
	GLuint m_vertexArrayObject;
	GLuint m_vertexArrayBuffer;

	unsigned int m_drawCount;

public:
	Mesh(Vertex * verts, unsigned int numVerts);

	void Draw();

	~Mesh();
};

#endif //	MESH_H