#version 120

in vec2 texCoord;
uniform sampler2D diffuse;
uniform vec4 uColor = vec4(1,1,1,1);


void main() 
{
    vec4 ul =  texture2D(diffuse, vec2(texCoord.x,texCoord.y));
    ul *= uColor;
    gl_FragColor = ul;
}
