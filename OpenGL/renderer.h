#include "display.h"
#include "Text.h"
#include "gameobject.h"
#include <vector>

#pragma once
class Renderer
{
public:
	static Renderer* getInstance();
	int window_Width;
	int window_Height;
	Display* display;
	std::vector<GameObject*> gameObjects;
	std::vector<Text*> texts;

	int Render(int height, int width);
	~Renderer();
private:
	static Renderer* instance;
};

